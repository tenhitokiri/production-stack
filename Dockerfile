FROM mysql:5.7

RUN { \
    echo '[mysqld]'; \
    echo 'character-set-server=utf8'; \
    echo 'collation-server=utf8_general_ci'; \
    echo 'sql_mode = STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION':\
    echo '[client]'; \
    echo 'default-character-set=utf8'; \
    } > /etc/mysql/conf.d/charset.cnf

EXPOSE 3306
CMD ["mysqld"]
