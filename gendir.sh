# make base directory and set permissions
sudo mkdir -p /data && sudo chmod 777 /data

#create letsencrypt and portainer directories
sudo mkdir -p /data/{letsencrypt,configs,portainer/{logs,data}}

#create mysql and pbx directories
sudo mkdir -p /data/mysql/{lib,settings} 

#Create the directories for the pbx
sudo mkdir -p /data/pbx/{certs,logs,data,html}

#Create the directories for the gitlab
sudo mkdir -p /data/gitlab/{config,logs,data}

#Create the directories for the docker registry
sudo mkdir -p /data/registry

#create mongodb directory
sudo mkdir -p /data/mongodb 

#create symlinks to prometheus and grafana directories
sudo ln -s ~/dev/monitor/prometheus/ /data/
sudo ln -s ~/dev/monitor/grafana/ /data/

#create MegaCli directory and backup directory
sudo mkdir -p /data/{megasync,backup}

#sudo mkdir -p /data/{letsencrypt,gitlab/{config,logs,data},portainer/{logs,data},mysql/{lib,settings},mongodb,registry,pbx/{certs,logs,data,html}} 
#
# add regexp=".*\\.refineria\\.lan\$" forward-to=192.168.99.2