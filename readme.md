# Pre Requisitos

## Instalar Docker y Docker-Compose

Instalar dependencias

        sudo apt update && sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg 

Agregar llave gpg

         curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - 

Agregar repositorio para ubuntu 18.04

         sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" 

Agregar repositorio para ubuntu 20.04

         sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" 

Instalar

         sudo apt update && sudo apt install docker-ce docker-compose 

Verificar

         sudo systemctl status docker

## Preparar el entorno de trabajo

Ejecutar el archivo `gendir.sh` para crear los directorios de data persistente. Modificar según se requiera.

# Configuración de Docker Registry con UI

## Comandos para subir

Ejecutar `docker images` para saber el id de la imagen

Con el Id de la imagen ejecutar `docker tag id direccion_ip:puerto/nombre_imaven:version`

Finalmente ejecutar `docker push direccion_ip:puerto/nombre_imaven:version`

## Solucionar error "HTTP response to HTTPS client"

Crear o modificar archivo en el CLIENTE /etc/docker/daemon.json

        { "insecure-registries":["myregistry.example.com:5000"] }

Reiniciar el Demonio de Docker

    sudo service docker restart

## Docker Stack
sudo docker stack deploy -c "archivo.yml" "nombre" 